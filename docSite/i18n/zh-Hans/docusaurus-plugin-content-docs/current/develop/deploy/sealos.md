---
sidebar_position: 1
---

# Sealos 一键部署

无需服务器、无需魔法、无需域名，点击即可部署 👇

[![](https://raw.githubusercontent.com/labring-actions/templates/main/Deploy-on-Sealos.svg)](https://cloud.sealos.io/?openapp=system-fastdeploy%3FtemplateName%3Dfastgpt)

由于需要部署数据库，部署完后需要等待 2~4 分钟才能正常访问。

## 运行
